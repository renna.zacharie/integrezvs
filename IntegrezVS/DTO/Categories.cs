﻿namespace DTO
{
    public enum Categories
    {
        Gastronomy, Activities, Events, Outdoors, Culture, Lifestyle, BonValaisan
    }
}