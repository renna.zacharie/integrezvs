﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Criteria
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public Categories Category { get; set; }
        public int Score { get; set; }
        public Criteria(int id, string text, Categories category, int score)
        {
            Id = id;
            Text = text;
            Category = category;
            Score = score;
        }
    }
}
