﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
            var cont = document.getElementsByClassName("collcontent");
            for (var j = 0; j < cont.length; j++) {
                if (cont[j].getAttribute("id") !== content.getAttribute("id") && cont[j].style.display === "block") {
                    cont[j].style.display = "none";
                    cont[j].previousElementSibling.classList.remove("active");
                }
            }
        }
    });
}