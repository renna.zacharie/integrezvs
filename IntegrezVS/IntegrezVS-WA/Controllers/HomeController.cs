﻿using BLL;
using IntegrezVS_WA.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace IntegrezVS_WA.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICriteriaManager _criteriaManager;

        public HomeController(ILogger<HomeController> logger, ICriteriaManager criteriaManager)
        {
            _logger = logger;
            _criteriaManager = criteriaManager;
        }

        public IActionResult Index()
        {
            return View(_criteriaManager.Criterias);
        }

        public IActionResult CalculateScores(int cid, bool status, int score, string activeCategory)
        {
            HttpContext.Session.SetString("ActiveCategory", activeCategory);
            if (status)
            {
                HttpContext.Session.Remove(cid.ToString());
            }
            else
            {
                HttpContext.Session.SetInt32(cid.ToString(), score);
            }
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}