﻿using DAL;
using DTO;

namespace BLL
{
    public class CriteriaManager : ICriteriaManager
    {
        public Criteria[] Criterias
        {
            get { return CriteriaDB.Instance.Criterias; }
        }
    }
}