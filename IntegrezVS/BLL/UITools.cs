﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class UITools
    {
        public static string Translate(string key)
        {
            string? rv = Translations.ResourceManager.GetString(key);
            if (rv == null)
            {
                rv = $"{key} NOT FOUND";
            }
            return rv;
        }
    }
}
