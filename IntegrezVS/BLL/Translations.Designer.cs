﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Translations {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Translations() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BLL.Translations", typeof(Translations).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activités.
        /// </summary>
        internal static string Activities {
            get {
                return ResourceManager.GetString("Activities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Etre un bon Valaisan.
        /// </summary>
        internal static string BonValaisan {
            get {
                return ResourceManager.GetString("BonValaisan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Evénements.
        /// </summary>
        internal static string Events {
            get {
                return ResourceManager.GetString("Events", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gastronomie.
        /// </summary>
        internal static string Gastronomy {
            get {
                return ResourceManager.GetString("Gastronomy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mode de vie et mentalité locale.
        /// </summary>
        internal static string Lifestyle {
            get {
                return ResourceManager.GetString("Lifestyle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Espaces naturels.
        /// </summary>
        internal static string Outdoors {
            get {
                return ResourceManager.GetString("Outdoors", resourceCulture);
            }
        }
    }
}
