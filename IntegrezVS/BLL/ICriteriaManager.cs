﻿using DTO;

namespace BLL
{
    public interface ICriteriaManager
    {
        Criteria[] Criterias { get; }
    }
}
