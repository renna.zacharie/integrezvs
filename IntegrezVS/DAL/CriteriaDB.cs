﻿using DTO;

namespace DAL
{
    public class CriteriaDB
    {
        private static CriteriaDB _instance;
        private List<Criteria> _criterias;

        private CriteriaDB()
        {
            _criterias= new List<Criteria>();
            ImportCriterias();
        }

        public static CriteriaDB Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CriteriaDB();
                }
                return _instance;
            }
        }

        public Criteria[] Criterias
        {
            get { return _criterias.ToArray(); }
        }

        private void ImportCriterias()
        {
            _criterias.Add(new Criteria(0, "Assiette valaisanne", Categories.Gastronomy, 3));
            _criterias.Add(new Criteria(1, "Raclette", Categories.Gastronomy, 5));
            _criterias.Add(new Criteria(2, "Fondue", Categories.Gastronomy, 2));
            _criterias.Add(new Criteria(3, "Brisolée", Categories.Gastronomy, 1));
            _criterias.Add(new Criteria(4, "Glareyarde", Categories.Gastronomy, 3));
            _criterias.Add(new Criteria(5, "Chasse", Categories.Gastronomy, 1));
            _criterias.Add(new Criteria(6, "Abricots du Valais", Categories.Gastronomy, 2));
            _criterias.Add(new Criteria(7, "Vins", Categories.Gastronomy, 3));
            _criterias.Add(new Criteria(8, "Bières", Categories.Gastronomy, 1));
            _criterias.Add(new Criteria(9, "Abricotine", Categories.Gastronomy, 5));
            _criterias.Add(new Criteria(10, "Williamine", Categories.Gastronomy, 2));
            _criterias.Add(new Criteria(11, "Sirops Morand", Categories.Gastronomy, 3));
            _criterias.Add(new Criteria(12, "Randonnées", Categories.Activities, 3));
            _criterias.Add(new Criteria(13, "Ski", Categories.Activities, 3));
            _criterias.Add(new Criteria(14, "After-ski", Categories.Activities, 3));
            _criterias.Add(new Criteria(15, "Associations (fot, fanfares, jeunesses)", Categories.Activities, 3));
            _criterias.Add(new Criteria(16, "Via-Ferrata", Categories.Activities, 1));
            _criterias.Add(new Criteria(17, "Escalade", Categories.Activities, 1));
            _criterias.Add(new Criteria(18, "VTT", Categories.Activities, 1));
            _criterias.Add(new Criteria(19, "Road-trip (jusqu'aux frontières du canton)", Categories.Activities, 1));
            _criterias.Add(new Criteria(20, "Matchs du FC Sion et HC Sierre", Categories.Activities, 3));
            _criterias.Add(new Criteria(21, "Lotos de sociétés", Categories.Activities, 1));
            _criterias.Add(new Criteria(22, "Foire du Valais", Categories.Events, 5));
            _criterias.Add(new Criteria(23, "Sainte-Catherine", Categories.Events, 1));
            _criterias.Add(new Criteria(24, "Festivals", Categories.Events, 2));
            _criterias.Add(new Criteria(25, "Carnaval", Categories.Events, 3));
            _criterias.Add(new Criteria(26, "Marche des sépages", Categories.Events, 2));
            _criterias.Add(new Criteria(27, "Caves ouvertes", Categories.Events, 3));
            _criterias.Add(new Criteria(28, "Racl'Agettes", Categories.Events, 2));
            _criterias.Add(new Criteria(29, "Combats de reine", Categories.Events, 3));
            _criterias.Add(new Criteria(30, "Fête du poulet", Categories.Events, 1));
            _criterias.Add(new Criteria(31, "Cervin", Categories.Outdoors, 3));
            _criterias.Add(new Criteria(32, "Lac Bleu", Categories.Outdoors, 1));
            _criterias.Add(new Criteria(33, "Glacier Aletsch", Categories.Outdoors, 2));
            _criterias.Add(new Criteria(34, "Barrage de la Grand-Dixence", Categories.Outdoors, 2));
            _criterias.Add(new Criteria(35, "Pyramides d'Euseigne", Categories.Outdoors, 1));
            _criterias.Add(new Criteria(36, "Lac Souterrain St-Léonard", Categories.Outdoors, 2));
            _criterias.Add(new Criteria(37, "Vigne de Farinet", Categories.Outdoors, 1));
            _criterias.Add(new Criteria(38, "Bisse d'Ayent", Categories.Outdoors, 1));
            _criterias.Add(new Criteria(39, "La Pisse-Vache", Categories.Outdoors, 1));
            _criterias.Add(new Criteria(40, "Lac du Bouveret", Categories.Outdoors, 2));
            _criterias.Add(new Criteria(41, "Valère et Tourbillon", Categories.Culture, 3));
            _criterias.Add(new Criteria(42, "Tschäggättä du Loetschental", Categories.Culture, 1));
            _criterias.Add(new Criteria(43, "Musée du vin", Categories.Culture, 2));
            _criterias.Add(new Criteria(44, "Fondation Gianadda", Categories.Culture, 1));
            _criterias.Add(new Criteria(45, "Barryland", Categories.Culture, 1));
            _criterias.Add(new Criteria(46, "Abbaye de St-Maurice", Categories.Culture, 1));
            _criterias.Add(new Criteria(47, "Rouler en Sub bleue avec des étoiles", Categories.BonValaisan, 3));
            _criterias.Add(new Criteria(48, "Habiter dans le fond d'une vallée", Categories.BonValaisan, 1));
            _criterias.Add(new Criteria(49, "Manger et boire de bons produits locaux", Categories.BonValaisan, 3));
            _criterias.Add(new Criteria(50, "Porter une casquette avec le logo des installations locales", Categories.BonValaisan, 2));
            _criterias.Add(new Criteria(51, "Avoir un carnotz", Categories.BonValaisan, 3));
            _criterias.Add(new Criteria(52, "Faire son vin et sa viande séchée", Categories.BonValaisan, 3));
            _criterias.Add(new Criteria(53, "Acheter un passeport valaisan", Categories.BonValaisan, 2));
            _criterias.Add(new Criteria(54, "Détester les autres cantons suisses", Categories.Lifestyle, 5));
        }
    }
}